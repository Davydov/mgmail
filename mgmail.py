import urllib, urllib2, cookielib
import random
import struct
import logging
import pprint

def tf(s):
    if s == 'T':
        return True
    else:
        return False

class Gmail:
    base_url = 'https://mail.google.com/mail/m/'
    p = '1.1'

    def __init__(self, user, password, debug=False):
        self.user = user
        self.password = password
        self.debug = debug
        self.cj = cookielib.CookieJar()
        self.opener = urllib2.build_opener(
            urllib2.HTTPCookieProcessor(self.cj))
        self.opener.addheaders = [('User-agent',
                                   'mgmail'
                                   '(http://bitbucket.org/Davydov/mgmail/)')]
        urllib2.install_opener(self.opener)

    def url(self):
        '''Returns random url work with'''
        return self.base_url + str(random.randint(1000, 90000))

    def read_values(self, lines, store, names, convert=None):
        if convert is None:
            convert = [str] * len(names)
        for i in xrange(len(names)):
            store[names[i]] = convert[i](lines.pop(0))

    def read_array(self, num, lines, store, names, convert=None):
        for i in xrange(num):
            entry = {}
            self.read_values(lines, entry, names, convert)
            store.append(entry)


    def login(self):
        '''Perform login'''
        params = urllib.urlencode({'zym': 'l', 'user': self.user,
                                   'password': self.password, 'p': self.p})

        f = urllib2.urlopen(self.url(), params)
        return self.parse_search(f)

    def category(self, category, per_page=50, start=0):
        params = urllib.urlencode({'s': 'cat', 'cat': category, 'sz': per_page,
                                   'st': start, 'p': self.p})

        f = urllib2.urlopen(self.url(), params)
        return self.parse_search(f)
    

    def parse_search(self, f):
        reply = self.parse(f)
        status = reply.pop(0)
        if status == 'E': # Error
            err_type = reply.pop(0)
            err_string = reply.pop(0)
            #TODO throw exception, captcha
            print err_type, err_string
            return
        elif status == 'T':
            r = {}
            self.read_values(reply, r,
                             ('id_code', 'start_thread', 'num_threads',
                              'total_threads', 'num_unread'),
                             (str, int, int, int, int, int))
            r['threads'] = []
            self.read_array(r['num_threads'], reply, r['threads'],
                            ('is_read', 'is_starred', 'has_attachments',
                             'from', 'subject', 'time', 'url'),
                            (tf, tf, tf, str, str, str, str))
            for thread in r['threads']:
                thread['id'] = thread['url'].split('=', 1)[1]
            self.read_values(reply, r, ('num_labels',), (int,))
            r['labels'] = []
            self.read_array(r['num_labels'], reply, r['labels'],
                            ('label', 'num_unread'), (str, int))

            if self.debug:
                logging.debug('Server:\n' + pprint.pformat(r))
            
            return r
    
    def get_thread(self, id, message=''):
        params = urllib.urlencode({'th': id, 'v': 'c', 'p': self.p})
        if message is not None:
            params += '&d=u&n=%s' % message
        f = urllib2.urlopen(self.url(), params)
        reply = self.parse(f)
        status = reply.pop(0)
        if status == 'E': # Error
            #TODO make error handler
            err_type = reply.pop(0)
            err_string = reply.pop(0)
            #TODO throw exception, captcha
            print err_type, err_string
            return
        elif status == 'C':
            r = {}
            self.read_values(reply, r,
                             ('thread_id', 'thread_subject', 'num_messages'),
                             (str, str, int))
            r['messages'] = []
            for i in xrange(r['num_messages']):
                message = {}
                # The doc is wrong.
                self.read_values(reply, message,
                            ('is_expanded', 'is_starred', 'has_attachments',
                             'from_friendly', 'to_friendly', 'date_friendly',
                             'from_address', 'b1', 'to_address', 'b2', 'b3',
                             'timestamp', 'subject', 'url', 'b4',
                             'from_address2','to_address2'),
                            (tf, tf, tf, str, str, str, str, str, str, str,
                             str, str, str, str, str, str, str))
                message['number'] = message['url'].split('=')[-1].split('#')[0]
                message['id'] = message['url'].split('#m_')[1]
                if message['is_expanded']:
                    message['body'] = ''
                    while True:
                        try:
                            line = reply.pop(0)
                        except IndexError:
                            break
                        if not line.startswith(':'):
                            reply.insert(0, line)
                            break
                        else:
                            message['body'] = message['body'] + line[1:] + '\n'
                    if message['has_attachments']:
                        self.read_values(reply, message, ('num_attachments',),
                                         (int,))
                        message['attachments'] = []
                        self.read_array(message['num_attachments'], reply,
                                        message['attachments'],
                                    ('filename', 'id', 'mimetype'))
                r['messages'].append(message)

            if self.debug:
                logging.debug('Server:\n' + pprint.pformat(r))

            return r
            

    def parse(self, f):
        '''Parse binary format returned by gmail mobile'''
        r = []
        while True:
            b = f.read(2)
            if len(b)<2:
                break
            l = struct.unpack('!H', b)[0]
            if l == 0:
                break
            r.append(f.read(l))
        return r

if __name__ == '__main__':
    import sys
    import getpass
    try:
        user, password = open('.password').read().strip().split(':',1)
    except:
        if len(sys.argv) < 2:
            user = raw_input('Username:')
        else:
            user = sys.argv[1]
        password = getpass.getpass()
    g = Gmail(user, password)
    d = g.login()
    d = g.category('Chats')
    for thread in d['threads']:
        print thread['id'], thread['subject']
    
        
    thread = g.get_thread('1247b719646fa3a8', '0')
    print thread

    for message in thread['messages']:
        print message['subject']
        if message['is_expanded']:
            print message['body']
#         if message['is_expanded']:
#             print message['body']
#         #n = g.get_thread(thread['thread_id'], message['id'])
#         #print '='*10
#         #for message in n['messages']:
#         #    if message['is_expanded']:
        #        print message['body']


