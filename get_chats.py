#!/usr/bin/python
import sys
import getpass
import optparse
import time
import random
import os.path
from email.mime.text import MIMEText
from email.Header import Header
from email.utils import getaddresses
import re
import logging
import hashlib
import urllib2

import mgmail


html_re =  re.compile(r'<a href="[^"]*">([^<]*)</a>')

def remove_html_tags(data):
    return html_re.sub(r'\1', data)

def sleep(min, max):
    sec = random.randint(min, max)
    logging.debug('Sleep %s sec.' % sec)
    time.sleep(sec)

def format_message(message, timezone, userhash):
    eml = MIMEText(remove_html_tags(message['body']), 'plain', 'UTF-8')
    eml['From'] = encode_field(message['from_address'])
    eml['To'] = encode_field(message['to_address'])
    eml['Date'] = message['timestamp'] + ' ' + timezone
    eml['Subject'] = Header(message['subject'], 'UTF-8')
    eml['Message-ID'] = '<%s_%s@gmailchats.myths.ru>' % (message['id'],
                                                         userhash)
    return eml.as_string()

def encode_field(field):
    '''Encode from/to/cc/bcc field.'''
    addresses = getaddresses([field])
    r = []
    for name, email in addresses:
        if name:
            r.append('%s <%s>' % (Header(name, 'UTF-8'), email))
        else:
            r.append(email)
    return ', '.join(r)


def main():
    # Parse options.
    usage = 'usage: %prog [options] username'
    parser = optparse.OptionParser(usage)
    parser.add_option('-s', '--start', type='int', dest='start', default=0,
                      help='start with START thread.')
    parser.add_option('-t', '--timezone', dest='timezone', default='+0000',
                      help='timezone in form +nnnn (default: +0000); '
                      'server returns date in local time.')
    parser.add_option('-o', '--outdir', dest='outdir', default='.',
                      help='directory to save messages (default ./).')
    parser.add_option('-n', '--new', action='store_true',
                      dest='new', default=False,
                      help='get only new messages '
                      '(exit on first already downloaded thread). '
                      'Use this if you want just to update your archive.')
    parser.add_option('-p', '--password-file', dest='password_file',
                      default=None, help='file containing password.')
    parser.add_option('-d', '--debug', action='store_const',
                      dest='loglevel', const=logging.DEBUG,
                      default=logging.INFO, help='show debug messages.')
    parser.add_option('-r', '--raw', action='store_true',
                      dest='raw', default=False,
                      help='show raw server communications '
                      '(works only in debug mode).')
    (options, args) = parser.parse_args(sys.argv[1:])
    if len(args) != 1:
        parser.error('You should specify username.')

    # Enable logging.
    logging.basicConfig(format='%(levelname)s\t%(message)s',
                        level=options.loglevel)

    # Username and password processing.
    user = args[0]
    # userhash is used to create unique Message-ID.
    userhash = hashlib.sha1(user).hexdigest()
    if options.password_file is not None:
        try:
            password = open(options.password_file).read().strip('\n')
        except:
            parser.error("Couldn't read password from file.")
    else:
        password = getpass.getpass()

    g = mgmail.Gmail(user, password, options.raw)
    d = g.login()
    start = options.start
    donefile = options.outdir + '/' + '.done'

    # Get list of completed threads.
    try:
        done = {}
        for line in open(donefile):
            i, s = line.strip().split('=', 1)
            done[i] = s
    except IOError:
        done = {}

    # Number of threads per page.
    per_page = 100
    # Number of downloaded threads.
    counter = 0
    exit = False
    while True:
        r = g.category('Chats', per_page, start=start)
        if len(r['threads']) == 0:
            # No messages. Exiting.
            break
        for thread in r['threads']:
            if thread['id'] in done and \
                    done[thread['id']] == thread['time']:
                logging.debug('Already got %s.' % thread['id'])
                if options.new:
                    logging.debug('No more new threads.')
                    exit = True
                    break
            else:
                logging.info('Downloading thread %s: %s' % (thread['id'],
                                                thread['subject']))
                this_thread = g.get_thread(thread['id'], '0')
                for message in this_thread['messages']:
                    fn = '%s/%s_%s.eml' % (options.outdir,
                                           thread['id'],
                                           message['id'])
                    logging.debug('File: %s.' % fn)
                    f = open(fn, 'w')
                    f.write(format_message(message, options.timezone,
                                           userhash))
                    f.close()
                    counter += 1
                f = open(donefile, 'a')
                print >> f, '%s=%s' % (thread['id'], thread['time'])
                f.close()
                sleep(5, 10)
        if exit:
            break
        sleep(5,30)
        start += per_page
        logging.info('Getting message from %s.' % start)

    logging.info('Finished. %d message(s) saved.' % counter)


if __name__ == '__main__':
    try:
        main()
    except urllib2.URLError, e:
        logging.error('Network error occured: %s.' % str(e))
    except urllib2.HTTPError, e:
        logging.error('Communication error occured: %s.' % str(e))
        logging.info('Try again later.')
